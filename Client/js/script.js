var BASE_URL = '';
var USER = false;
var ROOMS = [];
var OPEN_ROOM = '';
$(document).ready(function() {
    $('body').on('contextmenu', '#canvas', function(e) {
        return false;
    });
    $('input[name="submit-name"]').click(function() {
        USER = $('input[name="name"]').val();
        var usernameisokay = checkUsernamevalidity(USER);
        if (usernameisokay) {
            socket.emit('checkUsername', USER);
        } else {
            $('.errorMsg').show();
            $('.errorMsg').html("Please only use the characters a-z and 0-9 in your name");
        }
        socket.on('checkUsernameAnswer', function(ignAvailable) {
            if (ignAvailable) {
                $('.login-screen').hide();
                $('.errorMsg').hide();
                renderMatchList();
            } else {
                $('.errorMsg').show();
                $('.errorMsg').html("Oops! " + USER + " is already taken!");
            }
        });
        return false;
    });
    $('body').on('click', '.create-room', createRoom);
    $('body').on('click', '.room', showRoom);
    $('body').on('click', '.back-to-lobby', backToLobby);
    $('.modal').modal();
});

function showRoom() {
    var clickedRoom = $(this).attr('id');
    console.log(clickedRoom);
    $.get(BASE_URL + '/room/' + clickedRoom, function(room) {
        renderRoom(room);
    });
}

function backToLobby() {
    console.log("remove match ending");
    $('.scoreboardarea').hide().removeClass('match-ending');
    $('#matchmaking').show();
    leaveRoom(OPEN_ROOM);
    OPEN_ROOM = '';
    $('.back-to-lobby').hide();
}

function createRoom() {
    var roomName = $('input[name="room-name"]').val();
    var max_players = $('input[name="max-players"]').val();
    var win_condition = $('input[name="win-condition"]').val();
    if (roomName !== '' && win_condition !== '' && win_condition >= 0 && win_condition <= 100 && max_players !== '' && max_players >= 2 && max_players <= 12 ) {
        $.post(BASE_URL + '/rooms', {
            user: USER,
            name: roomName,
            max_players: max_players,
            win_condition: win_condition
        }, function(room) {
            $('#open-room .unready-room').hide();
            $('#open-room .ready-room').show();
            renderRoom(room);
        });
        socket.emit('joinGame', {
            roomName: roomName,
            playerName: USER
        });
    } else {
        $('.matches-msg').text('Please enter a name');
    }
    return false;
}

function checkUsernamevalidity(username) {
    var re = /^[a-z0-9]+$/i;
    var OK = re.exec(username);
    if (!OK) {
        console.log('Some characters used were not allowed');
        return false;
    }
    return true;
}

function renderMatchList() {
    $('.match-list').show();
    $.get(BASE_URL + '/rooms', function(data) {
        console.log(data.length);
        if (data.length === 0) {
            $('.matches-msg').text('No games, go ahead and create one');
            $('.room-list').html("");
        } else {
            $('.matches-msg').text('');
            $.each(data, function(index, room) {
                renderRoomInList(room);
            });
        }
    });
}

function renderRoomInList(room) {
    var exists = $('.room-list').find('#' + room.name);
    var template = $('<p class="room" id="' + room.name + '">Room name: ' + room.name + '<br>Players: ' + room.players.length + '/' + room.max_players + '<br>Room Status: ' + room.status + '</p>');
    if (exists.length) {
        $('.room-list').html(template);
    } else {
        $('.room-list').append(template);
    }
}

function renderRoom(room) {
    var playerNames = [];
    var is_in_room = false;
    OPEN_ROOM = room.name;
    $room = $('#open-room');
    $room.show();
    console.log("show room");
    $('#open-room .room-name').html("<h2>" + room.name + "</h2>");
    $('#open-room .player-list').html('');
    $('#open-room .room-info .players-current').html(room.players.length);
    $('#open-room .room-info .players-max').html(room.max_players);
    $room.find('.room-info .win-condition').html(room.win_condition);

    $.each(room.players, function(index, player) {
        if (player.name === USER) {
            is_in_room = true
        }
        playerNames.push(player.name);
        var readyText = (player.ready) ? ' - READY' : ' - NOT READY';
        $('#open-room .player-list').append('<p>' + player.name + readyText + '</p>');
    });
    if (playerNames.indexOf(USER) >= 0) {
        $('#open-room .join-room').hide();
        $('#open-room .leave-room').show();
        $room.addClass('joined-room');
    } else {
        $('#open-room .join-room').show();
        $('#open-room .leave-room').hide();
        $room.removeClass('joined-room');
    }
    if (room.status == "Running") {
        $('.room-controls').hide();
    } else {
        $('.room-controls').show();
    }
    $('#open-room .join-room').unbind('click').click(function() {
        joinRoom(room.name);
        
    });
    $('#open-room .leave-room').unbind('click').click(function() {
        leaveRoom(room.name);
    });
    $('#open-room .ready-room').unbind('click').click(function() {
        readyRoom(room.name);
        
    });
    $('#open-room .unready-room').unbind('click').click(function() {
        unreadyRoom(room.name);
        
    });
}

function joinRoom(name) {
    $.post(BASE_URL + '/rooms/join', {
        user: USER,
        name: name
    }, function(room) {
        renderMatchList();
        renderRoom(room);
    });
    socket.emit('joinGame', {
        roomName: name,
        playerName: USER
    });
    $('#open-room .unready-room').hide();
    $('#open-room .ready-room').show();
}

function rematch(name) {
    $.post(BASE_URL + '/rooms/rematch', {
        user: USER,
        name: name
    }, function(room) {
        renderMatchList();
        renderRoom(room);
    });
    socket.emit('rematch', {
        roomName: name,
        playerName: USER
    });
}

function leaveRoom(name) {
    $.post(BASE_URL + '/rooms/leave', {
        user: USER,
        name: name
    }, function(room) {
        renderMatchList();
        renderRoom(room);
        OPEN_ROOM = '';
        $('#open-room').hide();
    });
    socket.emit('leaveGame', {
        roomName: name,
        playerName: USER
    });
    $('.scoreboardarea').hide().removeClass('match-ending');
    $('#open-room').removeClass('joined-room');
    $('#open-room .unready-room').hide();
    $('#open-room .ready-room').hide();
}

function readyRoom(name) {
    $.post(BASE_URL + '/rooms/ready', {
        user: USER,
        name: name
    }, function(room) {
        renderMatchList();
        renderRoom(room);
    });
    $('#open-room .ready-room').hide();
    $('#open-room .unready-room').show();
}

function unreadyRoom(name) {
    $.post(BASE_URL + '/rooms/unready', {
        user: USER,
        name: name
    }, function(room) {
        renderMatchList();
        renderRoom(room);
    });
    $('#open-room .unready-room').hide();
    $('#open-room .ready-room').show();
}
socket.on('update_room', function(room) {
    if (USER) {
        renderMatchList();
        if (room !== undefined) {
            if (room.name == OPEN_ROOM) {
                renderRoom(room);
            }
        }
    }
});