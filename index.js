var express = require('express')();
var server = require('http').Server(express);
var bodyParser = require('body-parser');
var io = require('socket.io')(server);
var now = require("performance-now");
ID = require('./js/id.js');
//resources
var player = require('./js/player.js').player;
var game = require('./js/game.js').game;
var prod = require('./js/prod.js').prod;
var meteor = require('./js/meteor.js').meteor;
var helper = require('./js/helperFunctions.js');
//Server variables
games = []; //global variable
var connectedPlayers = [];
//Rooms
var rooms = [];
io.on('connection', function(socket) {
    var id = ID.id();
    console.log('a ' + id + ' connected ');
    var player_data = [];
    player_data['socket_id'] = id;
    player_data['socket'] = socket;
    socket.on('checkUsername', function(newUsername) {
        console.log(newUsername);
        var ignAvailable = true;
        for (var i = connectedPlayers.length - 1; i >= 0; i--) {
            if (connectedPlayers[i].name == newUsername) {
                ignAvailable = false;
                console.log(newUsername + " was taken...");
            }
        }
        socket.emit('checkUsernameAnswer', ignAvailable);
        if (ignAvailable) {
            player_data['thisplr'] = new player(id, newUsername);
            connectedPlayers.push(player_data['thisplr']);
            socket.emit('assignName', newUsername);
        }
    });
    socket.on('joinGame', function(data) {
        socket.join(data.roomName);
        socket.on('leaveGame', function(data) {
            socket.leave(data.roomName);
        });
        socket.on('input', function(inputData) {
            player_data['thisplr'].inputData = inputData;
        });
        socket_blink_handler(player_data);
        socket_meteor_handler(player_data);
        socket_prod_handler(player_data);
        socket_melee_handler(player_data);
    });
    socket.on('disconnect', function() {
        console.log(id + ' disconnected');
        for (var i = rooms.length - 1; i >= 0; i--) {
            for (var j = rooms[i].players.length - 1; j >= 0; j--) {
                if (rooms[i] !== undefined && player_data['thisplr'] !== undefined) {
                    if (player_data['thisplr'].name == rooms[i].players[j].name) {
                        rooms[i].players.splice(j, 1);
                        io.emit('update_room', rooms[i]);
                        if (rooms[i].players.length < 2) {
                            rooms.splice(i, 1);
                        }
                    }
                }
            }
        }
        for (var k = connectedPlayers.length - 1; k >= 0; k--) {
            if (connectedPlayers[k].id == id) {
                for (var i = games.length - 1; i >= 0; i--) {
                    for (var j = games[i].players.length - 1; j >= 0; j--) {
                        if (games[i].players[j].id == id) {
                            //Remove player from current if any
                            games[i].players.splice(j, 1);
                        }
                    }
                }
                //Remove player from global onlinelist
                connectedPlayers.splice(k, 1);
            }
        }
        io.emit(id + ' disconnected');
    });
});
function socket_melee_handler(player_data) {
    player_data['socket'].on('castMelee', function() {
        var wasCast;
        for (var i = games.length - 1; i >= 0; i--) {
            if (games[i].gameID == player_data['thisplr'].currentGame) {
                wasCast = player_data['thisplr'].castMelee(games[i]);
            }
        }
        if (wasCast) {
            player_data['socket'].emit('cooldownMelee', player_data['thisplr'].meleeCooldown);
        }
        wasCast = false;
    });
}

function socket_blink_handler(player_data) {
    player_data['socket'].on('castBlink', function(mousePosData) {
        var wasCast = player_data['thisplr'].castBlink(mousePosData);
        if (wasCast) {
            player_data['socket'].emit('cooldownBlink', player_data['thisplr'].blinkCooldown);
        }
        wasCast = false;
    });
}

function socket_meteor_handler(player_data) {
    player_data['socket'].on('castMeteor', function(mousePosData) {
        for (var i = games.length - 1; i >= 0; i--) {
            if (games[i].gameID == player_data['thisplr'].currentGame) {
                if (now() - player_data['thisplr'].lastCastMeteor >= player_data['thisplr'].meteorCooldown && player_data['thisplr'].dead === false) {
                    //Stand still while casting
                    var newmeteorid = ID.id();
                    var newmeteor = new meteor(player_data['socket_id'], player_data['thisplr'].name, mousePosData, player_data['thisplr'].color, now(), newmeteorid);
                    games[i].meteors.push(newmeteor);
                    player_data['thisplr'].lastCastMeteor = now();
                    player_data['socket'].emit('cooldownMeteor', player_data['thisplr'].meteorCooldown);
                    console.log("Meteor");
                }
            }
        }
    });
}
function socket_prod_handler(player_data) {
    player_data['socket'].on('prod', function(mousePosition) {
        if (now() - player_data['thisplr'].lastCastProd >= player_data['thisplr'].prodCooldown && player_data['thisplr'].dead === false) {
            var newProd = new prod(player_data['socket_id'], player_data['thisplr'].name, mousePosition, player_data['thisplr'].x + (player_data['thisplr'].width / 2), player_data['thisplr'].y + (player_data['thisplr'].height / 2), player_data['thisplr'].color, now());
            player_data['thisplr'].lastCastProd = now();
            for (var i = games.length - 1; i >= 0; i--) {
                if (games[i].gameID == player_data['thisplr'].currentGame) {
                    games[i].prods.push(newProd);
                }
            }
            console.log("prod created");
            player_data['socket'].emit('cooldownProd', player_data['thisplr'].prodCooldown);
        }
    });
}

function startRoom(roomData) {
    console.log("Start room: ", roomData);
    var gameid = ID.id();
    var newGame = new game(io, roomData, gameid);
    for (var i = connectedPlayers.length - 1; i >= 0; i--) {
        for (var j = roomData.players.length - 1; j >= 0; j--) {
            if (roomData.players[j].name == connectedPlayers[i].name) {
                newGame.players.push(connectedPlayers[i]);
            }
        }
    }
    for (var i = newGame.players.length - 1; i >= 0; i--) {
        newGame.players[i].currentGame = gameid;
    }
    games.push(newGame);
    newGame.start();
    io.to(roomData.name).emit('initGame', roomData);
}
//INITIATE
express.use(bodyParser.json());
express.use(bodyParser.urlencoded({
    extended: true
}));
express.get('/', function(req, res) {
    res.status(200).sendFile(__dirname + '/Client/index.html');
});

express.get('/js/:name', function(req, res) {
    res.status(200).sendFile(__dirname + "/Client/js/" + req.params.name);
    console.log(req.params.name + " sent");
});
express.get('/grafx/:name', function(req, res) {
    res.status(200).sendFile(__dirname + "/grafx/" + req.params.name);
    console.log(req.params.name + " sent");
});
express.get('/Client/dist/:name', function(req, res) {
    res.status(200).sendFile(__dirname + "/Client/dist/" + req.params.name);
    console.log(req.params.name + " sent");
});
express.get('/rooms', function(req, res) {
    res.json(rooms);
});
express.get('/players', function(req, res) {
    res.json(connectedPlayers);
});
express.get('/games', function(req, res) {
    res.json(games);
});
express.post('/rooms', function(req, res) {
    var roomTaken = false;
    for (var i = rooms.length - 1; i >= 0; i--) {
        if (rooms[i].name == req.body.name) {
            roomTaken = true;
        }
    }
    if (roomTaken === false) {
        rooms.forEach(function(room) {
            var index = 0;
            room.players.forEach(function(player) {
                if (player.name == req.body.user) {
                    room.players.splice(index, 1);
                    io.emit('update_room', room);
                }
                index++;
            });
        });
        var room = {
            name: req.body.name,
            players: [{
                name: req.body.user
            }],
            status: "Waiting",
            max_players: req.body.max_players,
            win_condition: req.body.win_condition
        };
        rooms.push(room);
        io.emit('update_room', room);
        res.json(room);
    }
    for (var i = rooms.length - 1; i >= 0; i--) {
        if (rooms[i].players.length === 0) {
            rooms.splice(i, 1);
        }
    }
});
express.get('/room/:name', function(req, res) {
    rooms.forEach(function(roomObj) {
        if (roomObj.name == req.params.name) {
            room = roomObj;
        }
    });
    res.json(room);
});
express.post('/rooms/:op(join|leave|rematch|ready|unready)', function(req, res) {
    var room = false;
    rooms.forEach(function(roomObj) {
        if (roomObj.name == req.body.name) {
            room = roomObj;
        }
    });
    switch (req.params.op) {
        case 'join':
            if (room.status !== 'Running') {
                rooms.forEach(function(roomObj) {
                    var index = 0;
                    var roomPlayers = [];
                    roomObj.players.forEach(function(player) {
                        if (player.name == req.body.user) {
                            roomObj.players.splice(index, 1);
                            console.log(player + "joined the room");
                            io.emit('update_room', roomObj);
                        }
                        index++;
                    });
                });
                room.players.push({
                    name: req.body.user,
                    ready: false
                });
                io.emit('update_room', room);
            }
            break;
        case 'leave':
            var index = 0;
            if (typeof room.players !== undefined) {
                room.players.forEach(function(player) {
                    if (player.name == req.body.user) {
                        room.players.splice(index, 1);
                    }
                    index++;
                });
            }
            io.emit('update_room', room);
            if (room.status === 'Running' && room.players.length < 2) {
                io.emit('stopGame', room);
            }
            break;
        case 'rematch':
            room.status = "Waiting";
            room.players.shift({
                name: req.body.user
            });
            rooms.forEach(function(roomObj) {
                var index = 0;
                var roomPlayers = [];
                roomObj.players.forEach(function(player) {
                    if (player.name == req.body.user) {
                        roomObj.players.splice(index, 1);
                        console.log(player);
                        io.emit('update_room', roomObj);
                    }
                    index++;
                });
            });
            room.players.push({
                name: req.body.user
            });
            io.emit('update_room', room);
            break;
        case 'ready':
            var countReady = 0;
            room.players.forEach(function(player) {
                if (player.name == req.body.user) {
                    player.ready = true;
                }
                if (player.ready) {
                    countReady++;
                }
            });
            if (countReady == room.players.length && room.players.length > 1) {
                room.status = 'Running';
                startRoom(room);
            }
            io.emit('update_room', room);
            break;
        case 'unready':
            var countReady = 0;
            room.players.forEach(function(player) {
                console.log(player);
                console.log(req.body.user);
                if (player.name == req.body.user) {
                    player.ready = false;
                }
            });
            io.emit('update_room', room);
            break;
    }
    for (var i = rooms.length - 1; i >= 0; i--) {
        if (rooms[i].players.length === 0) {
            rooms.splice(i, 1);
        }
    }
    io.emit('update_room');
    res.json(room);
});
server.listen(5000, function() {
    console.log('listening on *:5000');
});